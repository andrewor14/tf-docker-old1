#!/bin/bash

# =========================================================================
#  A helper script to output hostnames of all containers on the hosts
#  specified by the host file. The output format is one hostname per line.
# =========================================================================

if [[ "$#" != "2" ]]; then
  echo "Usage: get_container_hostnames.sh [host_file] [output_directory]"
  exit 1
fi
SCRIPT_DIR="$(dirname $(realpath "$0"))"

# Check if host file exists
HOST_FILE="$1"
if [[ ! -f "$HOST_FILE" ]]; then
  echo "ERROR: $HOST_FILE does not exist"
  exit 1
fi

# Check if output directory exists
OUTPUT_DIRECTORY="$2"
OUTPUT_HOST_FILE="$OUTPUT_DIRECTORY/hosts.txt"
OUTPUT_ADDRESS_FILE="$OUTPUT_DIRECTORY/ip.txt"
if [[ ! -d "$OUTPUT_DIRECTORY" ]]; then
  echo "ERROR: $OUTPUT_DIRECTORY does not exist"
  exit 1
fi
if [[ "$OUTPUT_HOST_FILE" == "$HOST_FILE" ]]; then
  echo "ERROR: You almost overwrote your own host file ($HOST_FILE)"
  exit 1
fi

# Drop the last address, which corresponds to the network endpoint (not a container)
IP_ADDRESS_COMMAND="sudo docker network inspect virtual-net | grep IPv4 | sed '\$d'"
IP_ADDRESS_COMMAND="$IP_ADDRESS_COMMAND | awk '{print \$2}' | sed 's/\"\(.*\)\/.*/\1/g'"
"$SCRIPT_DIR"/run_command_on_all.sh "$HOST_FILE" "$IP_ADDRESS_COMMAND" |\
  grep -v "Connection\|Warning" | sed 's/\r//g' > "$OUTPUT_ADDRESS_FILE"
echo "Wrote to $OUTPUT_ADDRESS_FILE, contents are:"
cat "$OUTPUT_ADDRESS_FILE"

# Assume hostnames are the same as the container IDs
HOSTNAME_COMMAND="sudo docker container ls | tail -n +2 | awk '{print \$1}'"
"$SCRIPT_DIR"/run_command_on_all.sh "$HOST_FILE" "$HOSTNAME_COMMAND" |\
  grep -v "Connection\|Warning" | sed 's/\r//g' > "$OUTPUT_HOST_FILE"
echo "Wrote to $OUTPUT_HOST_FILE, contents are:"
cat "$OUTPUT_HOST_FILE"

