#!/bin/bash

# ================================================================
#  A helper script that pulls the latest updates in the following
#  repos, re-installing tensorflow and horovod if necessary.
# ================================================================

DOCKER_DIR="${DOCKER_DIR:=/root/dev/tf-docker}"
MODELS_DIR="${MODELS_DIR:=/root/dev/models}"
HOROVOD_DIR="${HOROVOD_DIR:=/root/dev/horovod}"

# Make sure the directories exist
if [[ ! -d "$DOCKER_DIR" ]]; then
  echo "ERROR: $DOCKER_DIR does not exist"
  exit 1
fi
if [[ ! -d "$MODELS_DIR" ]]; then
  echo "ERROR: $MODELS_DIR does not exist"
  exit 1
fi
if [[ ! -d "$HOROVOD_DIR" ]]; then
  echo "ERROR: $HOROVOD_DIR does not exist"
  exit 1
fi

# Pull each repo and re-install tensorflow and horovod
cd "$DOCKER_DIR"
git pull origin virtual | grep 'up to date'
TF_WHEEL="$(ls | grep tensorflow.*whl)"
pip3 uninstall -y tensorflow tensorflow-gpu
pip3 install "$TF_WHEEL"
cd "$MODELS_DIR"
git pull origin virtual-new
cd "$HOROVOD_DIR"
HOROVOD_UP_TO_DATE="$(git pull origin virtual | grep 'up to date')"
./build_and_install.sh

echo "Everything up to date"

